//By given an integer value n Write a C Program to print multiplication tables from 1 to n12

#include<stdio.h>

int main()
{
    int x,i=1,y;
    printf("Enter the Number:");
    scanf("%d",&x);

    for (i=1;i<=x;i++)
    {
        for(y=i;y<=i*x;y=y+i)
        {
            printf("|%d|\t",y);
        }
        printf("\n");
    }
    return 0;
}
