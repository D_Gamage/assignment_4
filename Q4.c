//Write a C program to find factors of a given number
#include<stdio.h>

int main()
{
    int x,y,i;
    printf("Enter the Number:");
    scanf("%d",&x);
    printf("Factors of %d are:\n",x);
    for(y=1;y<=x;y++)
    {
        i=x%y;
        if (i==0)
        printf("\t \t%d \n",y);
    }
}
